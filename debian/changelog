rainloop (1.16.0+dfsg-1) unstable; urgency=high

  [ Daniel Ring ]
  * SECURITY NOTICE:
    In versions of Rainloop prior to 1.14.0, the "hide_passwords = On"
    configuration option does not work correctly. This may cause plaintext
    passwords to be stored in log files when logging is enabled.

    Note that the default configuration provided with this package disables
    logging; only users who have explicitly enabled it will be affected.

    For more information, see the upstream issue on GitHub:
    https://github.com/RainLoop/rainloop-webmail/issues/1872

    This issue is fixed in Rainloop 1.14.0 and later. (Closes: #962629)

  * Update include paths of dependences (Closes: #997726)
  * Depend on php-predis instead of php-nrk-predis (Closes: #998863)
  * DFSG repack to remove vendored dependencies
  * DFSG repack PHP libraries already in Debian
  * Use system libjs-jquery-ui
  * New upstream version 1.14.0+dfsg-1
  * Update node-opentip library path

  [ Yadd ]
  * Update standards version to 4.6.0, no changes needed.
  * New upstream version 1.16.0+dfsg
  * Refresh patches
  * Require node-opentip ≥ 2.4.6-3~

 -- Daniel Ring <dring@wolfishly.me>  Thu, 09 Dec 2021 06:24:53 +0100

rainloop (1.14.0-3) unstable; urgency=medium

  * Team upload

  [ Debian Janitor ]
  * Set debhelper-compat version in Build-Depends.
  * Update renamed lintian tag names in lintian overrides.
  * Set upstream metadata fields: Bug-Submit.

  [ Xavier Guimard ]
  * Add fix for node-knockout-sortable ≥ 1.2.0~
  * Bump debhelper compatibility level to 13
  * Declare compliance with policy 4.5.1

 -- Xavier Guimard <yadd@debian.org>  Mon, 21 Dec 2020 18:32:04 +0100

rainloop (1.14.0-2) unstable; urgency=medium

  * Update bundled gulp-less for node-less 3.11.2 (Closes: #962037)
  * Update build paths for node-autolinker (Closes: #962039)

 -- Daniel Ring <dring@wolfishly.me>  Mon, 15 Jun 2020 18:45:48 -0700

rainloop (1.14.0-1) unstable; urgency=medium

  * New upstream release
  * Update build to use Webpack 4 and Babel 7 (Closes: #933481, #960482)
  * Use unversioned virtual packages for suggests (Closes: #930524)
  * Remove dependencies on deprecated packages (Closes: #935428, #951023)

 -- Daniel Ring <dring@wolfishly.me>  Sat, 16 May 2020 15:46:04 -0700

rainloop (1.12.1-2) unstable; urgency=medium

  * Install default config files to /etc (Closes: #920674)
  * Remove symlinks to default config files on uninstall

 -- Daniel Ring <dring@wolfishly.me>  Wed, 06 Feb 2019 23:59:52 -0800

rainloop (1.12.1-1) unstable; urgency=medium

  * New upstream release

 -- Daniel Ring <dring@wolfishly.me>  Sun, 13 Jan 2019 00:30:06 -0800

rainloop (1.11.1-2) unstable; urgency=medium

  * Depend on php-nrk-predis instead of libphp-predis (Closes: #917605)
  * Update default webserver config files for PHP-FPM 7.3

 -- Daniel Ring <dring@wolfishly.me>  Sat, 05 Jan 2019 01:06:30 -0800

rainloop (1.11.1-1) unstable; urgency=medium

  * Initial release (Closes: #861581)

 -- Daniel Ring <dring@wolfishly.me>  Mon, 17 Dec 2018 20:42:00 -0700
